function [png] = sunbattery_pdf()
png.PN_name = 'sunbattery';
png.set_of_Ps = {'pBattery'};
png.set_of_Ts = {'tSun', 'tLight'};
png.set_of_As = {'tSun', 'pBattery', 1, ...
				'pBattery', 'tLight', 1};
