clear all; clc;

global global_info;
global_info.STOP_AT = 1;

global timeinfo;
timeinfo.timedelta = 90;

global PN;

png = pnstruct('sunbattery_pdf');

dyn.m0 = {'pBattery', 0};
dyn.ft = {'tSun', 1/24, 'tLight', 2/24};

pni = initialdynamics(png, dyn);

sim = gpensim(pni);
prnss(sim);
plotp(sim, {'pBattery'});
