clear all; clc;

global global_info;
global_info.STOP_AT = 48;

global ticker;
ticker.time = 0;

global PN;

png = pnstruct('waterReservoir_pdf');

dyn.m0 = {'pWaterReservoir', 0,'pBattery', 0};
dyn.ft = {'tRain', 1,'tGen',2, 'tHouse', 4};

pni = initialdynamics(png, dyn);

sim = gpensim(pni);
prnss(sim);
plotp(sim, {'pWaterReservoir','pBattery'});
