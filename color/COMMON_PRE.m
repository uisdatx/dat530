function [fire, transition] = COMMON_PRE(transition)
    fire = true;
    switch transition.name
    case 'tGen'
        transition.new_color = int2str(20);
        
    case 'tDestroyer'
        tokens = tokenAnyColor('pTemp', 2, {'0'});
        fire = false;
    case 'tFeedBack'
        tokens = tokenWOAnyColor('pTemp', 2, {'0'});
        transition.selected = tokens
        fire = all(tokens);
    end
end