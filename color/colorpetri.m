clear all; clc;

global global_info;
global_info.STOP_AT = 10;


global PN;

startTime = now();

png = pnstruct('colorpetri_pdf');

dyn.m0 = {};

dyn.ft = {'tGen', 1, ...
        'tFeedBack', 1, ...
        'tConsumer', 1, ...
        'tDestroyer', 1, ...
        'allothers', 1};

pni = initialdynamics(png, dyn);
sim = gpensim(pni);
prnss(sim);
prnfinalcolors(sim);
%cotree(pni, 1, 1);

disp("Done at: ")
(now() - startTime)*100000

plotp(sim, {'pTokens', 'pTemp'});

