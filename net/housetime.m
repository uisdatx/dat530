function [sleeptime] = housetime(time)
    global PN;
    global energy;
    global timeinfo;

    % Adjust the timing to be on the new delta time
    x = PN.current_time + timeinfo.timedelta;
    
    % Adjust the power utilization for that time of day, since normal house holds have to peaks, 
    % one early in the morning and one later at evening.
    dayCycle = 1 / (sin(2*pi*(x-0.125)*2)/2+1);

    if (x - energy.lastUpdateTime >= 1)
        % We only update this value once a day since the distribution we have is for the mean temperature for one day.
        % The formula is also normalized with the values min of 20 and a mean of 27.18. It works by letting
        % low temperatures reduce the sleep time and high temperatures extending the sleep times. 
        % This results in the end that when there is cold more power is consumed and high temperatures result in less power use.
        energy.lastTempValue = (sin(2*pi*(x-114.72)/365)* 7.65 + 7.185 + normrnd(0,3.278) + 20) / 27.18;
        energy.lastUpdateTime = x;
        energy.allTempValues = [energy.allTempValues, energy.lastTempValue];
    end
    
    % The sleep time is adjusted based on a normal random variable, the time of day from the dayCycle and 
    % the calculated temperature for that day.
    sleeptime = max(normrnd(time, 10)/1000,0) * dayCycle * energy.lastTempValue;
    energy.houseTimes = [energy.houseTimes, sleeptime];
end