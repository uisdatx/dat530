# Information

## Sources

### Added references

-   https://www.fjordkraft.no/privat/stromforbruk/
-   http://www.ewea.org/wind-energy-basics/faq/
-   https://www.thewindpower.net/windfarm_en_13085_hog-jaeren-energipark.php
-   https://www.vindportalen.no/Vindportalen-informasjonssiden-om-vindkraft/Vindkraft/Vindkraft-i-Norge
-   https://www.solenergi.no/solstrm/
-   http://suncurves.com/no/v/17637/
-   https://www.ssb.no/kommunefakta/stavanger

### Not yet added references

## Consumption

| House type | kWh/year |
| ---------- | -------- |
| House      | 20 000   |
| Rekke hus  | 15 000   |
| Apartment  | 9 000    |

| House type | kWh/day |
| ---------- | ------- |
| House      | 54.80   |
| Rekkehus   | 41.10   |
| Apartment  | 26.66   |

## Generation

| Gen Type                       | MWh/year  |
| ------------------------------ | --------- |
| Water Turbine (Lysebotn 2)     | 1 500 000 |
| Wind turbine                   | 240 000   |
| Solar power (1 km<sup>2</sup>) | 170 000   |

| Gen Type                       | MWh/day |
| ------------------------------ | ------- |
| Water Turbine (Lysebotn 2)     | 4109.59 |
| Wind turbine                   | 652.53  |
| Solar power (1 km<sup>2</sup>) | 465.75  |

Nøkkeltall og begreper

-   Solceller leverer typisk 100–170 kWh strøm/m2 solcelleareal.
-   Et solcelleanlegg produserer ca. 700–1000 kWh per installert kWp per år.
-   Gj.snittlig skydekke 73 %
-   Gj.snittlig antall soltimer 11:41

---

## Forbruk

-   5 670 982.17 MWh

## Boliger (bebodde og ubebodde)

-   Innbyggere: 133410
-   2.18 Innbyggere/boenhet

| Hvor           | Type                               | Antall |
| -------------- | ---------------------------------- | ------ |
| 1103 Stavanger | Enebolig                           | 20 705 |
|                | Tomannsbolig                       | 10 849 |
|                | Rekkehus, kjedehus og andre småhus | 9 939  |
|                | Boligblokk                         | 16 797 |
|                | Bygning for bofellesskap           | 1 975  |
|                | Andre bygningstyper                | 1 024  |

| Type                               | Antall     | MWh/year | Total MWh/year | kWh/day          | kWh/h          | Sim Name |
| ---------------------------------- | ---------- | -------- | -------------- | ---------------- | -------------- | -------- |
| Enebolig                           | 20 705     | 20       | 414 100        | 1 134 520.55     | 47 271.69      | tHouse1  |
| Tomannsbolig                       | 10 849     | 20       | 216 980        | 594 465.75       | 24 769.41      | tHouse2  |
| Rekkehus. kjedehus og andre småhus | 9 939      | 15       | 149 085        | 408 378.08       | 17 015.75      | tHouse3  |
| Boligblokk                         | 16 797     | 9        | 151 173        | 408 452.05       | 17 018.84      | tHouse4  |
| Bygning for bofellesskap           | 1 975      | 9        | 17 775         | 48 698.63        | 2 029.11       | tHouse5  |
| Andre bygningstyper                | 1 024      | 9        | 9 216          | 25 249.32        | 1 052.04       | tHouse5  |
| **Total:**                         | **61 289** | **82**   | **958 329**    | **2 619 764.06** | **109 156.84** |

## Industri

## Our sims 5

| Gen Type                         | MWh/year  |
| -------------------------------- | --------- |
| Water Turbine                    | 1 500 000 |
| Wind turbine                     | 300 000   |
| Solar power (1 km<sup>2</sup>)++ | 200 000   |

| Gen Type              | MWh/year  | MWh/day     | MWh/h       |
| --------------------- | --------- | ----------- | ----------- |
| Water Turbine         | 1 500 000 | 4109,589041 | 171,2328767 | 172 |
| Wind turbine          | 300 000   | 821,9178082 | 34,24657534 | 34 |
| Solar power (1 km2)++ | 200 000   | 547,9452055 | 22,83105023 | 23 |

### Numbers

| Gen Type              | MWh/year    | MWh/day  | MWh/hour |
| --------------------- | ----------- | -------- | -------- |
| Water Turbine         | 1576800     | 4320     | 180      |
| Wind turbine          | 262800      | 720      | 30       |
| Solar power (1 km2)++ | 175200      | 480      | 20       |
| **SUM:**              | **2014800** | **5520** | **230**  |

| Type                               | Antall     | MWh/year | Total MWh/year | kWh/day       | kWh/h       | Sim Name |
| ---------------------------------- | ---------- | -------- | -------------- | ------------- | ----------- | -------- |
| Enebolig                           | 21 900     | 20       | 438 000        | 1 200 000     | 50 000      | tHouse1  |
| Tomannsbolig                       | 10 512     | 20       | 210 240        | 576 000       | 24 000      | tHouse2  |
| Rekkehus. kjedehus og andre småhus | 8 760      | 15       | 131 400        | 360 000       | 15 000      | tHouse3  |
| Boligblokk                         | 14 600     | 9        | 131 400        | 360 000       | 15 000      | tHouse4  |
| **Total:**                         | **55 722** | **64**   | **911 040**    | **2 496 000** | **104 000** |          |
