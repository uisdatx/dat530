function [time] = industrytime()
    global PN;
    global energy;
    global timeinfo;
    
    % Adjust the timing to be on the new delta time
    x = PN.current_time + timeinfo.timedelta;

    % Calculates the utilisation of the power grid for industry at time x. 
    % This function follows a usage pattern where it peaks mid day since 
    % that is the point of the day where most people are working.
    % This grap is also inverted since the peak utilization results in the
    % lowest waiting time, which is the part being adjusted
    inner = sin(2 * pi * (x - 0.25));
    industryTime = (abs(inner) + inner) / 2 + 0.5;
    time = 1 / industryTime / 20;

    % The values are also stored to a matrix to be able check the time delays
    energy.industryTimes = [energy.industryTimes, time];
    
end