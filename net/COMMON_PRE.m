
function [fire, transition] = COMMON_PRE(transition)

global timeinfo;
global PN;
global energy;

% Getting the current time and adjusting it with the number of days skiped in the year
curTime = PN.current_time + timeinfo.timedelta;

% The amount of wind in m/s needed to generate four 10 MWh tokens
windLimit = 0.722983257 * 4;

% Some comment

fire = false;

% Most of the pre conditions are for limiting the amount tokens places in a place
% This is done since no place should function as a battery, since a normal 
% power-grid does not have that functionality.
% They are also limited because if a building suddenly consumes a lot of power 
% from the network then a voltage drop could accure. Same as when when a heavy
% machinery starts and the lights in the room dims down. 
% if the power draw is to high, then this could result in some equipment failing.

% Since Solar and wind needs to be consumed when it is produces, the water generator has a lower check value
% to only generate tokens when solar and wind is not enough to supply the network

if (strcmpi(transition.name, 'tTransHV'))

    % Limit for the amount of tokens in the HV place.
    fire = ntokens('pHV') < 14;

elseif (strcmpi(transition.name, 'tSun'))

    % Solar panels only generate power in daylight which is represented by function.
    % The function represents the suns position in the sky, where values over 0 is daytime and under 0 is night time.
    % The first part is to limit the amount of energy the solar panels put into the grid.
    fire = (ntokens('pPreTrans') < 17) & ((sin(2*pi*(curTime-0.25)) + ( (sin(2*pi*(curTime-82)/365.2422)+1)/2*(18.5-6.18)/12 + 6.18/12 - 1) * sqrt(2))/2 > 0);

elseif (strcmpi(transition.name, 'tRain'))

    % Function for simulating rain fall into the reservoir.
    fire = gamrnd(0.36, 29.7) > 0;

elseif (strcmpi(transition.name, 'tWind'))

    % Calculating the amount of wind for every simulation tick once
    if (energy.lastCheckTime ~= PN.current_time) 
        energy.wind = energy.wind + (lognrnd(0.47, 0.7456) * 0.06);
        energy.count = energy.count + 1;
        energy.list = [energy.list, PN.current_time];
        fire = (ntokens('pPreTrans') < 17) & energy.wind > windLimit;
        if (energy.wind > windLimit)
            energy.wind = energy.wind - windLimit;
        end
        energy.lastCheckTime = PN.current_time;
    end

elseif (strcmpi(transition.name, 'tWaterGen'))
    fire = ntokens('pPreTrans') < 10;


% Basic limitations for house distribution places
% These values varies based on how may tokens are moved between transitions.
elseif strcmpi(transition.name, 'tTrans1')
    fire = ntokens('pDist1') < 13;
elseif strcmpi(transition.name, 'tTrans2')
    fire = ntokens('pDist2') < 13;
elseif strcmpi(transition.name, 'tTrans3')
    fire = ntokens('pDist3') < 4;
elseif strcmpi(transition.name, 'tTrans4')
    fire = ntokens('pDist4') < 4;

% Basic limitations for industry distribution places
elseif strcmpi(transition.name, 'tTrans5')
    fire = ntokens('pDist5') < 5;
elseif strcmpi(transition.name, 'tTrans6')
    fire = ntokens('pDist6') < 5;
elseif strcmpi(transition.name, 'tTrans7')
    fire = ntokens('pDist7') < 5;

% Sets the industry transisions to fire every time.
elseif strcmpi(transition.name, 'tIndustry1')
    fire = true;
elseif strcmpi(transition.name, 'tIndustry2')
    fire = true;
elseif strcmpi(transition.name, 'tIndustry3')
    fire = true;
else 
    fire = true;
end
end


    