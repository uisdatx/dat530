clear all; clc;

global global_info;
global_info.STOP_AT = 7; % The number of days for the simulation;
global_info.DELTA_TIME = 0.0025; % 400 cycles each day

% Current timing for the network is 7 days with 400 cycles every day. 1 / 0.0025 = 400


global timeinfo;
% Value for adjusting where to start in the year
timeinfo.timedelta = 170;
% Debug value
timeinfo.lastHouse1Fire = 0;

% All to global variables for controlling the power generation and consuption
global energy;
% current wind countent in the buffer waiting to be release as an energy token
energy.wind = 0;
% Debug variables to ensure that wind only runs in the pre file once.
energy.count = 0;
energy.list = [];

% Value for controlling that wind only runs once simulation tick
energy.lastCheckTime = -1;

% Debug value to verify the industry consumption at any given point
energy.industryTimes = [];

% Debug value to verify the houses consumption at any given point
energy.houseTimes = [];

% Values for controlling the temperature for one day, and limit to one temperature reading each day
energy.lastTempValue = 0;
energy.lastUpdateTime = -10;
energy.allTempValues = [];

global PN;

% Variable for taking the time of execution
startTime = now();

png = pnstruct('EntireNet_pdf');

% These variables are initialized to the threashold of the place + one consuption of the tokens.
% This is done to stabelize the network earlier, instead of having to wait
dyn.m0 = {'pPreTrans', 10, ...
         'pH1Empty', 1, ...
         'pH2Empty', 1, ...
         'pH3Empty', 1, ...
         'pH4Empty', 1, ...
         'pI1Empty', 1, ...
         'pI2Empty', 1, ...
         'pI3Empty', 1, ...
         'pDist1', 12, ... 
         'pDist2', 17, ... 
         'pDist3', 5, ...
         'pDist4', 5, ...
         'pDist5', 6, ...
         'pDist6', 6, ...
         'pDist7', 6, ...
         'pHV', 15};


dyn.ft = {'tRain', 1/24, ...
        'tSun', 1/(24 * 2), ...
        'tWind', .0025, ...
        'tWaterGen', 1/(24 * 3), ...
        'tTransHV', .0025, ...
        'tHouse1', 'housetime(18)', ... % Houses uses the housetime function to calculate 
        'tHouse2', 'housetime(12)', ... % a stochastic execution time for the trasnition
        'tHouse3', 'housetime(12)', ... 
        'tHouse4', 'housetime(12)', ... 
        'tIndustry1', 'industrytime()', ... % Industry uses the industrytime function to calculate 
        'tIndustry2', 'industrytime()', ... % a stochastic execution time for the trasnition
        'tIndustry3', 'industrytime()', ...
        'tTrans1', .0025, ... % Firing time set to the delta time for max firing speed
        'tTrans2', .0025, ...
        'tTrans3', .0025, ...
        'tTrans4', .0025, ...
        'tTrans5', .0025, ...
        'tTrans6', .0025, ...
        'tTrans7', .0025, ...
        'allothers', .1};

pni = initialdynamics(png, dyn);
sim = gpensim(pni);
,
%prnss(sim);
%cotree(pni, 1, 1);

endTime = (now() - startTime)*100000;
disp("Used Time: " + num2str(endTime) + "s => " + num2str(floor(endTime / 60)) + "m " + num2str(endTime - floor(endTime / 60) * 60) + "s");


figure(1);

% Plots for power in the power grid
subplot(4,1,1)
plotpdeb(sim, {'pPreTrans', 'pHV'});

% Plots for power distributions centers for the different house types
subplot(4,1,2)
plotpdeb(sim, {'pDist1', 'pDist2', 'pDist3', 'pDist4'});

% Plots for when the house is not able to fire
subplot(4,1,3)
plotpdeb(sim, {'pH1Empty', 'pH2Empty', 'pH3Empty', 'pH4Empty'});

% Plots for when the industry is not able to fire
subplot(4,1,4)
plotpdeb(sim, {'pI1Empty', 'pI2Empty', 'pI3Empty'});

figure(2);

subplot(3,1,1)
% Total Generation for the different generators
plotpdeb(sim, {'pWaterTot', 'pWindTot', 'pSunTot', 'pHVTot'});

subplot(3,1,2)
% Total consumation for the houses
plotpdeb(sim, {'pHouse1Tot', 'pHouse2Tot', 'pHouse3Tot', 'pHouse4Tot'});

subplot(3,1,3)
% Total Consumption for the industry buildings
plotpdeb(sim, {'pIndustry1Tot', 'pIndustry2Tot', 'pIndustry3Tot', });

figure(3)
subplot(2,1,1)
% Temperature curve for the mean values for the different days of the simulation
plot(1:global_info.STOP_AT, energy.allTempValues * 27.18 - 20)

subplot(2,1,2)
% If the temperature is under or over the mean value determing higher or lower power draw then mean value
plot(1:global_info.STOP_AT, [energy.allTempValues', (zeros(1,global_info.STOP_AT) + 1)'])


% Saving the simulation
disp("Save start");
save("RunWith_StartAt-" + num2str(timeinfo.timedelta) + "_Days-" + num2str(global_info.STOP_AT))
disp("Save end");
